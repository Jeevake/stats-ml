%**********************************************************************
% METHODS
%**********************************************************************

%--------------------------------------------------
\title[]
%--------------------------------------------------

%--------------------------------------------------
% TRANSITIONAL SLIDE
%--------------------------------------------------
\begin{frame}{}
%

    \begin{textblock*}{124mm}[0,0](2mm,20mm)  
        \begin{beamercolorbox}[sep=8pt,center]{title}
            \usebeamerfont{title} 
            Computational Physics
        \end{beamercolorbox}
    \end{textblock*}
    
    \begin{textblock*}{118mm}[0,0](5mm,40mm)  
        \textbf{Computational physics} is the study and implementation of numerical analysis to solve problems in physics for which a quantitative theory already exists.
    \end{textblock*}
    
    % note: this is in a different position that for "Condensed Matter Physics" because the above definition is only two lines (as opposed to three)
    \begin{textblock*}{64mm}[0,0](5mm,57.5mm)
        \textbf{Outline:}  
            \begin{itemize}
                \item \justifying First-principles simulations                
                \item \justifying High performance computing
                \item \justifying Methods development                  
%                \item \justifying (Better) Plasmonic materials (e.g., for solar energy conversion)
%                \item \justifying {\color{custom_gray} Solid state materials for photovoltaics}
        \end{itemize}
    \end{textblock*}

%    \begin{textblock*}{118mm}[0,0](71.875mm,42mm)
%        \includegraphics[scale=0.27]{SiH4_AIRSS_cropped}
%    \end{textblock*}

    % REFERENCES:
%    \textboxRefA{C.\ J.\ Pickard and R.\ J.\ Needs, \textit{Phys.\ Rev.\ Lett.} \textbf{97}, 045504 (2006)} 
    
%
\end{frame}

%--------------------------------------------------
\title[Computational Physics]
%--------------------------------------------------

%--------------------------------------------------
% THE ROLE OF COMPUTER SIMULATIONS
%--------------------------------------------------
\begin{frame}{The Role of Computer Simulations}
%\begin{frame}{The Connection between Experiment, Theory, and Computer Simulations}
%

    \begin{textblock*}{118mm}[0,0](27mm,13mm)
        \includegraphics[scale=0.3]{computational_physics/computational_physics}
    \end{textblock*}

    % REFERENCES:
    \textboxRefA{Adapted from: M.\ P.\ Allen and D.\ J.\ Tildesley, \textit{Computer Simulation of Liquids} (Oxford University Press: New York 1991)}
      
%
\end{frame}

%--------------------------------------------------
\input{computational_physics/first_principles_simulations}
%--------------------------------------------------

%--------------------------------------------------
\input{computational_physics/HPC}
%--------------------------------------------------

%--------------------------------------------------
\input{computational_physics/methods}
%--------------------------------------------------
